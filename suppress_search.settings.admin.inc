<?php
/**
 * @file
 * Page handlers for suppress search settings admin.
 */

/**
 * Manage Suppress Search settings.
 *
 * @ingroup forms
 * @see system_settings_form()
 */
function suppress_search_settings() {
  $form['suppress_search_search_form_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Search form submit path'),
    '#size' => 30,
    '#maxlength' => 60,
    '#default_value' => check_plain(variable_get('suppress_search_search_form_path', '')),
    '#description' => t('Change the path the core seach form submits queries to. For instance, for Apache Solr, this is \'search/apachesolr_search/\'. If left blank, the search form will continue to submit queries to core search.'),
  );

  $form['suppress_search_hide_content_tab'] = array(
    '#type' => 'checkbox',
    '#title' => t('Hide content tab'),
    '#default_value' => variable_get('suppress_search_hide_content_tab', ''),
    '#description' => t('Check this to hide the \'Content\' tab on the search results page. You must clear cache after changing this setting.'),
  );

  $form['suppress_search_hide_users_tab'] = array(
    '#type' => 'checkbox',
    '#title' => t('Hide users tab'),
    '#default_value' => variable_get('suppress_search_hide_users_tab', ''),
    '#description' => t('Check this to hide the \'Users\' tab on the search results page. You must clear cache after changing this setting.'),
  );

  $index_total = db_result(db_query('select count(sid) from {search_index}'));
  $dataset_total = db_result(db_query('select count(sid) from {search_dataset}'));
  $form['status'] = array('#type' => 'fieldset', '#title' => t('Empty search index'));
  $form['status']['status'] = array('#value' => t('<p>Core\'s search index can become very large on sites with a lot of content. Emptying this data can free up a lot of database space. However, If you want to re-enable core search at some later date, you WILL have to re-index all content from scratch.</p><p>Here are the current row counts for your search-related tables.</p><p>search_index: %index_total</p><p>search_dataset: %dataset_total</p>', array('%index_total' => $index_total, '%dataset_total' => $dataset_total)));
  $form['status']['wipe'] = array('#type' => 'submit', '#value' => t('Empty core search index'));

  return system_settings_form($form);
}

/**
 * Menu callback: confirm wiping of the index.
 */
function suppress_search_wipe_confirm() {
  return confirm_form(array(), t('Are you sure you want to empty core\'s search index?'), 'admin/settings/suppress_search', t('Core\'s search index WILL be emptied. If you want to re-enable core search at some later date, you WILL have to re-index all content from scratch. This action cannot be undone.'), t('Empty core search index'), t('Cancel'));
}

/**
 * Handler for wipe confirmation
 */
function suppress_search_wipe_confirm_submit(&$form, &$form_state) {
  if ($form['confirm']) {
    db_query('truncate {search_dataset}');
    db_query('truncate {search_index}');
    db_query('truncate {search_node_links}');
    db_query('truncate {search_total}');
    drupal_set_message(t('Core search index has been emptied.'));
    $form_state['redirect'] = 'admin/settings/suppress_search';
  }
}

/**
 * Validate callback.
 */
function suppress_search_settings_validate($form, &$form_state) {
  if ($form_state['values']['op'] == t('Empty core search index')) {
    drupal_goto('admin/settings/suppress_search/wipe');
  }
}
